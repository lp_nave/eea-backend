package lk.apiit.eea.assignment.workforce.testing;

import lk.apiit.eea.assignment.workforce.dto.comapny.CompanyCardDTO;
import lk.apiit.eea.assignment.workforce.services.company.CompanyService;
import lk.apiit.eea.assignment.workforce.services.company.RatingService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@RunWith(MockitoJUnitRunner.class)
class RatingServiceTest {

    private MockMvc mockMvc;

    @Mock
    RatingService ratingServiceMock;

    @InjectMocks
    CompanyService randomServiceMock;

    @Before
    public  void init(){
        System.out.println("Running innit");
        MockitoAnnotations.initMocks(this);
        randomServiceMock = new CompanyService();
        ratingServiceMock = new RatingService();

    }

    @Test
    void getRandom() throws Exception{
        init();
        List<CompanyCardDTO> list = new ArrayList<CompanyCardDTO>();

        Mockito.when(randomServiceMock.getRandom()).thenReturn(list);
        assertNotNull(randomServiceMock.getRandom());
    }


    @Test
    void setRatings() throws Exception {
        init();
        System.out.println("Testing");
        Mockito.when(ratingServiceMock.setRatings("21","3")).thenReturn(true);
        assertTrue(ratingServiceMock.setRatings("21","3"));
    }
}