package lk.apiit.eea.assignment.workforce.repos;

import lk.apiit.eea.assignment.workforce.model.Jobs;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface JobRepo extends CrudRepository<Jobs,String> {

    List<Jobs> findAllByOrderByJobIdDesc();

    List<Jobs> findAllByLocationContaining(String location);
    List<Jobs> findAllByNameContaining(String name);
//    List<Jobs> findAllByNameContainingOrLocationContaining(String searchkeyword, String location);
    List<Jobs> findAllByNameContainingAndLocationContaining(String name, String location);

    Long countByLocation(String location);

}
