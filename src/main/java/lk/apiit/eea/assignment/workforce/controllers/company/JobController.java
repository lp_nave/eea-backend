package lk.apiit.eea.assignment.workforce.controllers.company;

import lk.apiit.eea.assignment.workforce.dto.Job.Applicants;
import lk.apiit.eea.assignment.workforce.dto.Job.JobApplication;
import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.dto.Job.ListingsDTO;
import lk.apiit.eea.assignment.workforce.services.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class JobController {

    @Autowired
    private JobService jobService;

    @PostMapping("/addJob")
    public boolean addJob(@RequestBody JobDTO jobDTO)throws Exception{
        return jobService.addNewJobListing(jobDTO);
    }

    @PutMapping("/editJob")
    public boolean editJob(@RequestBody JobDTO jobDTO)throws Exception{
        return jobService.editJob(jobDTO);
    }

    @DeleteMapping("/deleteJob/{id}")
    public boolean deletejob(@PathVariable String id)throws Exception{
        return jobService.deleteJob(id);
    }

    @GetMapping("/searchJobs")
    public List<JobDTO> searchJobs(
            @RequestParam(value = "key",required = false) String key,
            @RequestParam(value = "loc", required = false) String loc)throws Exception{
        return jobService.search(key,loc);
    }

    @PostMapping("/applyForJob")
    public boolean applyForJob(@RequestBody JobApplication jobApplication)throws Exception{
        return jobService.applyForJob(jobApplication);
    }

    @GetMapping("/getApplicants/{email}")
    public List<Applicants> getAllApplicants(@PathVariable String email)throws Exception{
        return jobService.getAllApplicants(email);
    }

    @GetMapping("/getJob/{id}")
    public JobDTO getSelectedJob(@PathVariable String id )throws Exception{
        return jobService.getJob(id);
    }

    @GetMapping("/getAllJobs")
    public List<JobDTO> getJob()throws Exception{
        return jobService.getAllJobs();
    }

}
