package lk.apiit.eea.assignment.workforce.controllers.company;

import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.CompanyCardDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.RatingDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.RegisterCompanyDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.department.DepartmentDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.profile.CompanyProfileHeader;
import lk.apiit.eea.assignment.workforce.dto.comapny.profile.CompanySegment;
import lk.apiit.eea.assignment.workforce.dto.comapny.profile.ViewCompany;
import lk.apiit.eea.assignment.workforce.services.company.CompanyService;
import lk.apiit.eea.assignment.workforce.services.company.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @Autowired
    private RatingService ratingService;


    @GetMapping("/getRandomCompanies")
    public List<CompanyCardDTO> getRandom()throws Exception{
        return companyService.getRandom();
    }

    @GetMapping("/getCompanies")
    public List<CompanyCardDTO> getAll()throws Exception{
        return companyService.getAll();
    }

    @GetMapping("/searchCompany")
    public List<CompanyCardDTO> searchJobs(
            @RequestParam(value = "key", required = false) String key,
            @RequestParam(value = "loc", required = false) String loc)throws Exception{
        return companyService.search(key,loc);
    }

    @GetMapping("/getCompanyProfile/{email}")
    public CompanyProfileHeader getCompanyProfileHeader(@PathVariable String email)throws Exception{
        return companyService.getProfileHeader(email);
    }

    @GetMapping("/getCompanyDepartments/{email}")
    public List<DepartmentDTO> getDepartments(@PathVariable String email)throws Exception{
        return companyService.getDepartments(email);
    }

    @GetMapping("/getCompanyVacancies/{email}")
    public List<JobDTO> getVacancies(@PathVariable String email)throws Exception{
        return companyService.getVacancies(email);
    }
    //Used only by the mobile
    @GetMapping("/mobileCompanyVacancies/{id}")
    public List<JobDTO> getMobileCompanyVacancies(@PathVariable int id) throws Exception{
        return companyService.getMobileCompanyVacancie(id);
    }
//used by the mobile only
    @GetMapping("/mobileCompanyVacanciesByEmail/{email}")
    public List<JobDTO> getMobileVacancies(@PathVariable String email)throws Exception{
        return companyService.getMobileCompanyVacanciebyEmail(email);
    }

    @GetMapping("/viewCompany/{id}")
    public ViewCompany viewCompany(@PathVariable int id)throws Exception{
        return companyService.viewCompany(id);
    }

//    Used only by mobile
    @GetMapping("/viewCompanybyEmail/{email}")
    public ViewCompany viewCompany(@PathVariable String email)throws Exception{
        return companyService.viewCompanyByEmail(email);
    }

    @GetMapping("/companySegment/{email}")
    public CompanySegment companyprofileSegment(@PathVariable String email)throws Exception{
        return companyService.loadCompanyProfileSegment(email);
    }

    @PutMapping("/editCompany")
    public boolean editCompany(@RequestBody RegisterCompanyDTO dto)throws Exception{
        return companyService.editCompany(dto);
    }

    @GetMapping("/getCompany/{email}")
    public RegisterCompanyDTO getCompany(@PathVariable String email)throws Exception{
        return companyService.getCompany(email);
    }

//    @PostMapping("/setRating")
//    public boolean setRating(@RequestBody RatingDTO dto)throws Exception{
//        return ratingService.setRatings(dto);
//
//    }
    @GetMapping("/setRating/{id}/{rating}")
    public boolean setRating(@PathVariable String id, @PathVariable String rating)throws Exception{
        return ratingService.setRatings(id,rating);

    }
}
