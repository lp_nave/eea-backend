package lk.apiit.eea.assignment.workforce.dto.comapny.profile;

import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.department.DepartmentDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanySegment {
    private String companyName;
    private String location;
    private String image;
    private String imagePath;
    private List<DepartmentDTO> deparments;
    private List<JobDTO> jobs;
}
