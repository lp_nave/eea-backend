package lk.apiit.eea.assignment.workforce.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterClientDTO {
    private String email;
    private String firstName;
    private String lastName;
    private String contactNumber;
    private String location;
    private String password;

    public RegisterClientDTO(String firstName, String lastName, String contactNumber, String location) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.contactNumber = contactNumber;
        this.location = location;
    }
}
