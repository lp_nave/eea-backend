package lk.apiit.eea.assignment.workforce.services.admin;

import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.model.Jobs;
import lk.apiit.eea.assignment.workforce.repos.JobRepo;
import lk.apiit.eea.assignment.workforce.services.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AdminJobServices {

    @Autowired
    private JobRepo jobRepo;

    @Autowired
    private JobService jobService;

    public List<JobDTO> getAllJobs() {
        List<Jobs> jobs = (List<Jobs>) jobRepo.findAll();
        List<JobDTO> list = new ArrayList<>();

        jobs.forEach(j ->{

            JobDTO dto = new JobDTO();

            dto.setJobId(j.getJobId());
            dto.setName(j.getName());
            dto.setCompany(j.getDepartment().getCompany().getCompanyName());
            dto.setSalary(String.valueOf(j.getSalary()));
            dto.setLocation(j.getLocation());

            dto.setListedDate(j.getListedDate());

            list.add(dto);
        });

        return list;
    }

    public List<Jobs> getCSVdata() {
        List<Jobs> rawData  = (List<Jobs>) jobRepo.findAll();
        return rawData;
    }

    public boolean deleteJob(String id)throws Exception {
        try{
            jobService.deleteJob(id);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO DELETE JOB", e);
        }
    }
}
