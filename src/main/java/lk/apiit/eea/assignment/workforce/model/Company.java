package lk.apiit.eea.assignment.workforce.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "company")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "companyName")
    private String companyName;

    @Column(name = "address")
    private String address;

    @Column(name = "contactNumber")
    private String number;

    @Column(name = "location")
    private String location;

    @Column(name = "noOfEmployees")
    private int noOfEmployees;

    @Column(name = "decription")
    private String descpription;

    @Column(name = "noOfJobs")
    private int noOfJobs;

    @Column(name = "image")
    private String image;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "authentication_email", referencedColumnName = "email")
    private Authentication authentication;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "company")
    private List<Department> departmentList;

    @OneToOne(cascade = CascadeType.ALL, mappedBy ="company", orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Ratings ratings;

}
