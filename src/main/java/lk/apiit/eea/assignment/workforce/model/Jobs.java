package lk.apiit.eea.assignment.workforce.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "jobs")
public class Jobs {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "jobId")
    private String jobId;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "requirements")
    private String requirements;

    @Column(name = "type")
    private String type;

    @Column(name = "salary")
    private int salary;

    @Column(name = "listedDate")
    private Date listedDate;

    @Column(name = "location")
    private String location;

    @ManyToOne
    @JoinColumn(name = "department", referencedColumnName = "deptID")
    private Department department;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "myJobs")
    private Set<Client> applied;






}
