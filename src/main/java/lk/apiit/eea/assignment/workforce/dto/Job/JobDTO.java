package lk.apiit.eea.assignment.workforce.dto.Job;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JobDTO  implements  Comparable<JobDTO>{

    private String jobId;
    private String name;
    private String description;
    private String requirements;
    private String type;
    private String salary;
    private String deptId;
    private String location;
    private Date listedDate;
    private String company;

    public JobDTO(String jobId, String name, String description, String requirements, String type, String salary, String deptId, String location, Date listedDate) {
        this.jobId = jobId;
        this.name = name;
        this.description = description;
        this.requirements = requirements;
        this.type = type;
        this.salary = salary;
        this.deptId = deptId;
        this.location = location;
        this.listedDate = listedDate;
    }

    public JobDTO(String jobId, String name, String type, Date listedDate) {
        this.jobId = jobId;
        this.name = name;
        this.type = type;
        this.listedDate = listedDate;
    }

    public JobDTO(String name, String description, String requirements, String type, String salary, String deptId, String location) {
        this.name = name;
        this.description = description;
        this.requirements = requirements;
        this.type = type;
        this.salary = salary;
        this.deptId = deptId;
        this.location = location;
    }

    @Override
    public int compareTo(JobDTO jobDTO) {
        return getListedDate().compareTo(jobDTO.getListedDate());
    }
}
