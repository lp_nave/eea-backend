package lk.apiit.eea.assignment.workforce.dto.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientProfileSegmentDTO {

    private String firstName;
    private String lastName;
    private String location;
    private String contact;
    private String cvPath;
    private String image;
    private String imagePath;
    private long noOfJobsInLocation;
}
