package lk.apiit.eea.assignment.workforce.dto.client;

import lk.apiit.eea.assignment.workforce.dto.Job.ListingsDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ClientProfileDTO {
    private String firstName;
    private String lastName;
    private String email;
    private String contactNumber;
    private String location;
    private String image;
    private String imagePath;
    private List<ListingsDTO> joblist;
    private int sumJobs;

    public ClientProfileDTO(String firstName, String lastName, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
}
