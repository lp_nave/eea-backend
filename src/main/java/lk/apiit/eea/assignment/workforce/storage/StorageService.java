package lk.apiit.eea.assignment.workforce.storage;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;


public interface StorageService {

    void init();

    void store(MultipartFile file, String type, String fileName);

    Stream<Path> loadAll();

    Path load(String filename, String type);

    Resource loadAsResource(String filename, String type);

    void deleteAll();

}
