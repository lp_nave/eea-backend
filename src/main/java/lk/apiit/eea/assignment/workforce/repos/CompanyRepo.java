package lk.apiit.eea.assignment.workforce.repos;

import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CompanyRepo extends CrudRepository<Company,Integer> {
    @Query(value = "SELECT * FROM Company ORDER BY RAND() LIMIT 3", nativeQuery = true)
    List<Company> getRandom();

    Company findByEmail(String email);

    List<Company> findAllByCompanyNameContaining(String companyName);
    List<Company> findAllByLocationContaining(String location);

    List<Company> findAllByCompanyNameContainingAndLocationContaining(String companyName, String location);
}
