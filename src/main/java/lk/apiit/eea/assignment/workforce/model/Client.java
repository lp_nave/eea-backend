package lk.apiit.eea.assignment.workforce.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Client")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "clientId")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "contactNumber")
    private String contactNumber;

    @Column(name = "location")
    private String location;

    @Column(name = "cvPath")
    private String cvPath;

    @Column(name = "image")
    private String image;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "authentication_email", referencedColumnName = "email")
    private Authentication authentication;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "appliedJobs",
            joinColumns =  @JoinColumn(name = "clientId"),
            inverseJoinColumns = @JoinColumn(name = "jobId")
    )
    private Set<Jobs> myJobs;
}
