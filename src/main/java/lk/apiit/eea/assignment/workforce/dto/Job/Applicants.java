package lk.apiit.eea.assignment.workforce.dto.Job;

import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Applicants {
    private String jobId;
    private String jobName;
    private String department;
    private List<ClientProfileDTO> applicants;
}
