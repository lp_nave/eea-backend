package lk.apiit.eea.assignment.workforce.dto.comapny.profile;

import lk.apiit.eea.assignment.workforce.dto.Job.ListingsDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ViewCompany {
    private int id;
    private  String companyName;
    private String email;
    private String location;
    private String contactNumber;
    private  String address;
    private String image;
    private String imagePath;
    private int jobs;
    private int employee;
    private int departments;
    private int vacancies;
    private double rating;

    private List<ListingsDTO> joblist;
}
