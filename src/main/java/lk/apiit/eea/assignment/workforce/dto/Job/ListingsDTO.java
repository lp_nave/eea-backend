package lk.apiit.eea.assignment.workforce.dto.Job;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ListingsDTO {
    private String jobId;
    private String name;
    private String description;
    private String requirements;
    private String type;
    private int salary;
    private String companyName;
    private String companyDepartment;
    private String companyLocation;
    private Date listedDate;

    public ListingsDTO(String jobId, String name, String description, int salary, String companyName, String companyLocation, Date listedDate) {
        this.jobId = jobId;
        this.name = name;
        this.description = description;
        this.salary = salary;
        this.companyName = companyName;
        this.companyLocation = companyLocation;
        this.listedDate = listedDate;
    }


    //    public ListingsDTO(String jobId, String name, String description, String location, int salary, String companyName, Date listedDate) {
//    }
}
