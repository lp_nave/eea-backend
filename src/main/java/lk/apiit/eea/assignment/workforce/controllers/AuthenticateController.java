package lk.apiit.eea.assignment.workforce.controllers;

import lk.apiit.eea.assignment.workforce.dto.AuthenticateRequest;
import lk.apiit.eea.assignment.workforce.dto.client.RegisterClientDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.RegisterCompanyDTO;
import lk.apiit.eea.assignment.workforce.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticateController {

    private AuthenticationService authenticationService;

    @Autowired
    public AuthenticateController(AuthenticationService authenticationService){
        this.authenticationService = authenticationService;
        authenticationService.checkAdmin();
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticateUser(@RequestBody AuthenticateRequest auth) throws Exception{
        return authenticationService.authenticateUser(auth);
    }

    @PostMapping("/registerClient")
    public ResponseEntity<?> registerClient(@RequestBody RegisterClientDTO client)throws Exception{
        return authenticationService.addNewClient(client);
    }
    @PostMapping("/registerCompany")
    public ResponseEntity<?> registerCompany(@RequestBody RegisterCompanyDTO company)throws Exception{
        return authenticationService.addNewCompany(company);
    }

}
