package lk.apiit.eea.assignment.workforce.controllers.client;


import lk.apiit.eea.assignment.workforce.storage.StorageService;
import lk.apiit.eea.assignment.workforce.services.client.CVService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.nio.ch.IOUtil;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.Base64;


@RestController
public class CVController {

//    private final StorageService storageService;
//
//    @Autowired
//    public CSVController(StorageService storageService) {
//        this.storageService = storageService;
//    }

    @Autowired
    private StorageService storageService;

    @Autowired
    private CVService CVService;

    @PostMapping("/uploadCV/{email}")
    public boolean uploadCV(@PathVariable String email,@RequestParam("file") MultipartFile file )throws Exception{
        try{
            MultipartFile m = file;
            String newFileName= email+"CV.pdf";
            m.getOriginalFilename().replaceAll(m.getOriginalFilename(),newFileName);
            storageService.store(m,"CV", newFileName);
            CVService.saveFilename(email,newFileName);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO SAVE FILE", e);
        }

    }


    @GetMapping(value="/getCV/{email}", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> download(@PathVariable String email)throws Exception{
            String fileName = CVService.getFileName(email);
        Resource pdfFile = storageService.loadAsResource(fileName, "CV");
//        ClassPathResource pdfFile= new ClassPathResource("EEA_upload-dir/"+ fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.add("Access-Control-Allow-Origin", "*");
        headers.add("Access-Control-Allow-Methods", "GET, POST, PUT");
        headers.add("Access-Control-Allow-Headers", "Content-Type");
        headers.add("Content-Disposition", "filename=" + fileName);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");

        headers.setContentLength(pdfFile.contentLength());
        ResponseEntity<InputStreamResource> response = new ResponseEntity<InputStreamResource>(
                new InputStreamResource(pdfFile.getInputStream()), headers, HttpStatus.OK);
        return response;
    }
    @GetMapping(value = "/getPic")
    public String get(HttpServletResponse response) throws IOException {

        Resource pdfFile = storageService.loadAsResource("image.jpeg", "Profile");
        BufferedImage bImage = ImageIO.read(pdfFile.getFile());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(bImage, "jpeg", bos );
        byte [] data = bos.toByteArray();
        String convert = Base64.getEncoder().encodeToString(data);
//        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
//        return ResponseEntity.ok(data);
        return convert;


    }
}
