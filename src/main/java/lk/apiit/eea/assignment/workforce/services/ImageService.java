package lk.apiit.eea.assignment.workforce.services;

import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.repos.ClientRepo;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.storage.StorageService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.Base64;

@Service
public class ImageService {

    @Autowired
    private ClientRepo clientRepo;

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private StorageService storageService;

    public boolean saveFilename(String email, String role, String newFileName) throws Exception{
        try{
            if(role.equalsIgnoreCase("client")){
                Client c = clientRepo.findByEmail(email);
                c.setImage(newFileName);
                clientRepo.save(c);
                return true;
            }
            else if(role.equalsIgnoreCase("company")){
                Company company = companyRepo.findByEmail(email);
                company.setImage(newFileName);
                companyRepo.save(company);
                return true;
            }
            else return false;
        }catch (Exception e){
            throw new Exception("FAILED TO SAVE IMAGE", e);
        }

    }

    public String getB64(String fileName, String role) throws Exception{
        try{
            Resource image = storageService.loadAsResource(fileName, role);
            String exe = FilenameUtils.getExtension(fileName);
            FileInputStream fis = new FileInputStream(image.getFile());
            BufferedImage bImage = ImageIO.read(fis);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ImageIO.write(bImage, exe, bos );
            byte [] data = bos.toByteArray();
            String convert = Base64.getEncoder().encodeToString(data);
            return convert;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET THE IMAGE", e);
        }

    }
}
