package lk.apiit.eea.assignment.workforce.controllers;

import lk.apiit.eea.assignment.workforce.services.ImageService;
import lk.apiit.eea.assignment.workforce.storage.StorageService;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@RestController
public class ImageController {

    @Autowired
    private StorageService storageService;

    @Autowired
    private  ImageService imageService;

    @PostMapping("/uploadImage/{email}/{role}")
    public boolean uploadImage(
            @PathVariable String email,
            @PathVariable String role,
            @RequestParam("file") MultipartFile file )throws Exception{
        try{
            MultipartFile m = file;
            String extension = FilenameUtils.getExtension(file.getOriginalFilename());
            String newFileName= email+"Profile."+extension;
//            Files.copy(file.getInputStream(),)
//            m.getOriginalFilename().replaceAll(m.getOriginalFilename(),newFileName);
            storageService.store(m,"Profile", newFileName);
            boolean assign = imageService.saveFilename(email,role,newFileName);
            return assign;
        }
        catch (Exception e){
            throw new Exception("FAILED TO SAVE FILE", e);
        }
    }}
