package lk.apiit.eea.assignment.workforce.dto.comapny;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterCompanyDTO {
    private String email;
    private String companyName;
    private String address;
    private String contactNumber;
    private int noOfEmployees;
    private int noOfJobs;
    private String description;
    private String location;
    private String password;

    public RegisterCompanyDTO(String companyName, String address, String contactNumber, int noOfEmployees, int noOfJobs, String location) {
        this.companyName = companyName;
        this.address = address;
        this.contactNumber = contactNumber;
        this.noOfEmployees = noOfEmployees;
        this.noOfJobs = noOfJobs;
        this.location = location;
    }

    public RegisterCompanyDTO(String email, String companyName, String address, String contactNumber, int noOfEmployees, int noOfJobs, String description, String location) {
        this.email = email;
        this.companyName = companyName;
        this.address = address;
        this.contactNumber = contactNumber;
        this.noOfEmployees = noOfEmployees;
        this.noOfJobs = noOfJobs;
        this.description = description;
        this.location = location;
    }
}

