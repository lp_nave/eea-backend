package lk.apiit.eea.assignment.workforce.csv;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Jobs;

import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class WriteToCSV {

    public static void writeClientDataToCsv(PrintWriter writer, List<Client> clients) {
        String[] CSV_HEADER = { "email", "firstName", "lastName", "contactNumber","location", "Cv","image"};
        StatefulBeanToCsv<Client> beanToCsv = null;
        try (
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){
            csvWriter.writeNext(CSV_HEADER);

            //String lists

            for (Client client : clients) {
               
                String[] data = {
                        client.getEmail(),
                        client.getFirstName(),
                        client.getLastName(),
                        client.getContactNumber(),
                        client.getLocation(),
                        client.getCvPath(),
                        client.getImage(),
                };

                 csvWriter.writeNext(data);
            }

            // write List of Objects
//            ColumnPositionMappingStrategy<Client> mappingStrategy =
//                    new ColumnPositionMappingStrategy<Client>();
//
//            mappingStrategy.setType(Client.class);
//            mappingStrategy.setColumnMapping(CSV_HEADER);
//
//            beanToCsv = new StatefulBeanToCsvBuilder<Client>(writer)
//                    .withMappingStrategy(mappingStrategy)
//                    .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
//                    .build();
//
//            beanToCsv.write(clients);

            System.out.println("Write CSV using BeanToCsv successfully!");
        }catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }
    }

    public static void writeJobsDataToCsv(PrintWriter writer, List<Jobs> Joblist) {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");

        String[] CSV_HEADER = {
                "jobId",
                "name",
                "description",
                "requirements",
                "type",
                "salary LKR",
                "listedDate",
                "location",
                "department"
        };
        StatefulBeanToCsv<Jobs> beanToCsv = null;
        try (
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){

            csvWriter.writeNext(CSV_HEADER);

            for (Jobs job : Joblist) {

                String[] data = {
                        job.getJobId(),
                        job.getName(),
                        job.getDescription(),
                        job.getRequirements(),
                        job.getType(),
                        String.valueOf(job.getSalary()),
                        formatter.format(job.getListedDate()),
                        job.getLocation(),
                        job.getDepartment().getDeptName(),
                };

                csvWriter.writeNext(data);
            }

            System.out.println("Write CSV using BeanToCsv successfully!");
        }catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }
    }

    public static void writeCompanyDataToCsv(PrintWriter writer, List<Company> companylist) {
        String[] CSV_HEADER = {
                "id",
                "email",
                "companyName",
                "address",
                "contactNumber",
                "location",
                "noOfEmployees",
                "description",
                "noOfJobs",
                "image",
                "ratings",
        };
        StatefulBeanToCsv<Company> beanToCsv = null;
        try (
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){
            csvWriter.writeNext(CSV_HEADER);


            for (Company company : companylist) {

                String[] data = {
                        String.valueOf(company.getId()),
                        company.getEmail(),
                        company.getCompanyName(),
                        company.getAddress(),
                        company.getNumber(),
                        company.getLocation(),
                        String.valueOf(company.getNoOfEmployees()),
                        company.getDescpription(),
                        String.valueOf(company.getNoOfJobs()),
                        company.getImage(),
                        String.valueOf(company.getRatings().getRating())
                };

                csvWriter.writeNext(data);
            }

            System.out.println("Write CSV using BeanToCsv successfully!");
        }catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }
    }

}
