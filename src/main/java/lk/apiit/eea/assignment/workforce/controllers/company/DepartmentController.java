package lk.apiit.eea.assignment.workforce.controllers.company;

import lk.apiit.eea.assignment.workforce.dto.comapny.department.DepartmentDTO;
import lk.apiit.eea.assignment.workforce.services.company.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @PostMapping("/addDepartment")
    public boolean addDept(@RequestBody DepartmentDTO dept)throws Exception{
        return departmentService.addDept(dept);
    }

    @DeleteMapping("/deleteDepartment/{id}")
    public boolean deleteDept(@PathVariable String id)throws Exception{
        return departmentService.deleteDept(id);
    }

    @GetMapping("/getAllDepartments/{email}")
    public List<DepartmentDTO> getAll(@PathVariable String email)throws Exception{
        return departmentService.getAll(email);
    }
}
