package lk.apiit.eea.assignment.workforce.storage;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("storage")
public class StorageProperties {

    /**
     * Folder location for storing files
     */
    private String location = "EEA_upload-dir";

    private String profile = "EEA_Profiles";

    public String getLocation() {
        return location;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
