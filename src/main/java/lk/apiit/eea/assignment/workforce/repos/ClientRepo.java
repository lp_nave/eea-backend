package lk.apiit.eea.assignment.workforce.repos;

import lk.apiit.eea.assignment.workforce.model.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepo extends CrudRepository<Client, Integer> {
    Client findByEmail (String email);
}
