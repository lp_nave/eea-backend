package lk.apiit.eea.assignment.workforce.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "authentication")
public class Authentication {
    @Id
    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String role;

    @OneToOne(cascade = CascadeType.ALL, mappedBy ="authentication", orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Company company;

    @OneToOne(cascade = CascadeType.ALL, mappedBy ="authentication", orphanRemoval = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Client client;

}
