package lk.apiit.eea.assignment.workforce.services.client;

import lk.apiit.eea.assignment.workforce.dto.Job.ListingsDTO;
import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileDTO;
import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileSegmentDTO;
import lk.apiit.eea.assignment.workforce.dto.client.RegisterClientDTO;
import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.repos.ClientRepo;
import lk.apiit.eea.assignment.workforce.repos.JobRepo;
import lk.apiit.eea.assignment.workforce.services.ImageService;
import lk.apiit.eea.assignment.workforce.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ClientProfileService {

    @Autowired
    private ClientRepo clientRepo;

    @Autowired
    private JobRepo jobRepo;

    @Autowired
    private ImageService imageService;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    public ClientProfileSegmentDTO getSegment(String email) throws Exception {
        try{
            Client client = clientRepo.findByEmail(email);
            ClientProfileSegmentDTO dto = new ClientProfileSegmentDTO();

            if(client.getImage()!=null){
                String b64 = imageService.getB64(client.getImage(), "Profile");
                dto.setImage(b64);
                dto.setImagePath(client.getImage());
            }


            dto.setFirstName(client.getFirstName());
            dto.setLastName(client.getLastName());
            dto.setContact(client.getContactNumber());
            dto.setCvPath(client.getCvPath());
            dto.setLocation(client.getLocation());
//            dto.setNoOfJobsInLocation(client.getMyJobs().size());

            long count = jobRepo.countByLocation(client.getLocation());
            dto.setNoOfJobsInLocation(count);
            return dto;
        }
        catch (Exception e){
            throw new Exception("SEGMENT ERROR", e);
        }
    }

    public ClientProfileDTO getProfile(String email)throws Exception {
        try{
            Client client = clientRepo.findByEmail(email);
            ClientProfileDTO dto = new ClientProfileDTO();


            if(client.getImage()!=null){
                String b64 = imageService.getB64(client.getImage(), "Profile");
                dto.setImage(b64);
                dto.setImagePath(client.getImage());
            }

            dto.setFirstName(client.getFirstName());
            dto.setLastName(client.getLastName());
            dto.setContactNumber(client.getContactNumber());
            dto.setLocation(client.getLocation());
            dto.setEmail(client.getEmail());
            List<ListingsDTO> joblist = new ArrayList<>();

            client.getMyJobs().forEach(j -> {
                joblist.add(
                        new ListingsDTO(
                                j.getJobId(),
                                j.getName(),
                                j.getDescription(),
                                j.getSalary(),
                                j.getDepartment().getCompany().getCompanyName(),
                                j.getLocation(),
                                j.getListedDate()
                        )
                );
            });

            dto.setJoblist(joblist);

            return dto;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET PROFILE", e);
        }
    }

    public boolean editProfile(RegisterClientDTO dto) throws Exception{
        try{
            Client client = clientRepo.findByEmail(dto.getEmail());
            client.setFirstName(dto.getFirstName());
            client.setLastName(dto.getLastName());
            client.setContactNumber(dto.getContactNumber());
            client.setLocation(dto.getLocation());

            if(dto.getPassword()!=""){
                client.getAuthentication().setPassword(bcryptEncoder.encode(dto.getPassword()));
            }

            clientRepo.save(client);

            return true;
        }catch (Exception e){
            throw new Exception("FAILED TO EDIT PROFILE", e);
        }
    }

    public RegisterClientDTO getClient(String email) throws Exception{
        try{
            Client client = clientRepo.findByEmail(email);
            RegisterClientDTO dto = new RegisterClientDTO();

            dto.setEmail(client.getEmail());
            dto.setFirstName(client.getFirstName());
            dto.setLastName(client.getLastName());
            dto.setContactNumber(client.getContactNumber());
            dto.setLocation(client.getLocation());

            return dto;
        }
        catch(Exception e){
            throw new Exception("FAILED TO GET CLIENT",e);
        }
    }
}
