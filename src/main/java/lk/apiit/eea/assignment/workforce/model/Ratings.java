package lk.apiit.eea.assignment.workforce.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ratings")
public class Ratings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "rateID")
    private int id;

    @Column(name = "numberOfStars")
    private int starsReceived;

    @Column(name = "numberOfPeople")
    private int people;

    @Column(name = "rating")
    private double rating;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "ratedCompany", referencedColumnName = "id")
    private Company company;

}
