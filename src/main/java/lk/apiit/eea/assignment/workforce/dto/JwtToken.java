package lk.apiit.eea.assignment.workforce.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class JwtToken {
    private final String jwtToken;

    public JwtToken(String jwtToken) {
        this.jwtToken = jwtToken;
    }
}
