package lk.apiit.eea.assignment.workforce.controllers.admin;

import lk.apiit.eea.assignment.workforce.csv.WriteToCSV;
import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileDTO;
import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.services.admin.AdminClientServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class AdminClientController {

    @Autowired
    private AdminClientServices services;

    @GetMapping("adminClients")
    public List<ClientProfileDTO> getClients(){
        return services.getClients();
    }

    @GetMapping("/ClientsCSV")
    public void downloadClients(HttpServletResponse response) throws IOException{
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; file=customers.csv");

        List<Client> clients = services.getCSVdata();
        WriteToCSV.writeClientDataToCsv(response.getWriter(), clients);
    }

    @DeleteMapping("/DeleteClient/{email}")
    public boolean deleteClient(@PathVariable String email) throws Exception{
        return services.deleteClient(email);
    }
}
