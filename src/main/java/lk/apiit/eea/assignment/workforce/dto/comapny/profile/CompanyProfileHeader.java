package lk.apiit.eea.assignment.workforce.dto.comapny.profile;

import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyProfileHeader {
    private int id;
    private String companyName;
    private String location;
    private int jobs;
    private int employee;
    private int vacancies;
    private int departments;
    private double rating;
    private String image;
    private String imagePath;
    private List<JobDTO> jobList;

}
