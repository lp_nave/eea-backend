package lk.apiit.eea.assignment.workforce.services.admin;

import lk.apiit.eea.assignment.workforce.dto.comapny.CompanyCardDTO;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Department;
import lk.apiit.eea.assignment.workforce.model.Jobs;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.services.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AdminCompanyServices {

    @Autowired
    private CompanyRepo companyRepo;

    @Autowired
    private JobService jobService;

    public List<CompanyCardDTO> adminCompanies() {
        List<Company> companies = (List<Company>) companyRepo.findAll();
        List<CompanyCardDTO> list = new ArrayList<>();
        companies.forEach(company -> {
            CompanyCardDTO dto = new CompanyCardDTO();

            int vacancies=0;
            if(company.getDepartmentList()!=null){
                vacancies= company.getDepartmentList().stream().mapToInt(department -> department.getJobs().size()).sum();
            }

            dto.setCompanyId(company.getId());
            dto.setCompanyName(company.getCompanyName());
            dto.setLocation(company.getLocation());
            dto.setRating(company.getRatings().getRating());
            dto.setImagePath(company.getImage());
            dto.setJobs(vacancies);

            list.add(dto);
        });

        return list;
    }

    public List<Company> getCSVdata() {

        List<Company> rawData = (List<Company>) companyRepo.findAll();
        return rawData;
//        List<Company> list = new ArrayList<>();
//        rawData.forEach(company -> {
//            Company c = new Company();
//            c.setId(company.getId());
//            c.setEmail(company.getEmail());
//            c.set
//        });
    }

    public boolean deleteCompany(int id) throws Exception{
        try{
            Company c =  companyRepo.findById(id).orElse(null);
            List<Department> dl = c.getDepartmentList();
            for (Department d :dl){
                List<Jobs> jl = d.getJobs();
                for(Jobs j : jl){
                    jobService.deleteJob(j.getJobId());
                }
            }
            companyRepo.delete(c);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO DELETE COMPANY", e);
        }


    }
}
