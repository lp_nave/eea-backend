package lk.apiit.eea.assignment.workforce.services.client;

import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.repos.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CVService {

    @Autowired
    private ClientRepo clientRepo;

    public void saveFilename(String email,String fileName)throws Exception{
        try{
            Client c = clientRepo.findByEmail(email);
            c.setCvPath(fileName);
            clientRepo.save(c);
        }
        catch (Exception e){
            throw new Exception("DB FAILED TO SAVE PATH", e);
        }
    }

    public String getFileName(String email) throws Exception{
        try {
            Client client =clientRepo.findByEmail(email);
            return client.getCvPath();
        }
        catch (Exception e){
            throw new Exception("FALED TO GET PATH", e);
        }
    }
}
