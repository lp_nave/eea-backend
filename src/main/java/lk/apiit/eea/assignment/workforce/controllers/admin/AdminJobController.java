package lk.apiit.eea.assignment.workforce.controllers.admin;

import lk.apiit.eea.assignment.workforce.csv.WriteToCSV;
import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.model.Jobs;
import lk.apiit.eea.assignment.workforce.services.JobService;
import lk.apiit.eea.assignment.workforce.services.admin.AdminJobServices;
import lk.apiit.eea.assignment.workforce.services.company.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class AdminJobController {

    @Autowired
    private AdminJobServices services;

    @Autowired
    private DepartmentService departmentService;

    @GetMapping("/adminJobs")
    public List<JobDTO> adminJobs(){
        return services.getAllJobs();
    }

    @GetMapping("/JobCSV")
    public void downloadJobs(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; file=customers.csv");

        List<Jobs> jobs = services.getCSVdata();
        WriteToCSV.writeJobsDataToCsv(response.getWriter(), jobs);
    }

    @DeleteMapping("/DeleteJob/{id}")
    public boolean deleteJob(@PathVariable String id)throws Exception{
        return services.deleteJob(id);
    }
     @GetMapping("/Alldepartments")
    public ResponseEntity<?> getAllDepartments(){
        return ResponseEntity.ok().body(departmentService.listAll());
     }
}
