package lk.apiit.eea.assignment.workforce.services;

import lk.apiit.eea.assignment.workforce.dto.Job.Applicants;
import lk.apiit.eea.assignment.workforce.dto.Job.JobApplication;
import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.dto.Job.ListingsDTO;
import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileDTO;
import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Department;
import lk.apiit.eea.assignment.workforce.model.Jobs;
import lk.apiit.eea.assignment.workforce.repos.ClientRepo;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.repos.DepartmentRepo;
import lk.apiit.eea.assignment.workforce.repos.JobRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


@Service
@Transactional
public class JobService {

    @Autowired
    private JobRepo jobRepo;

    @Autowired
    private DepartmentRepo departmentRepo;

    @Autowired
    private ClientRepo clientRepo;

    @Autowired
    private CompanyRepo companyRepo;

    public boolean addNewJobListing(JobDTO jobDTO)throws Exception{
        try{
            Department dept = (Department) departmentRepo.findById(jobDTO.getDeptId()).orElse(null);

            Jobs newJob = new Jobs();
                    newJob.setName(jobDTO.getName());
                    newJob.setDescription(jobDTO.getDescription());
                    newJob.setRequirements(jobDTO.getRequirements());
                    newJob.setType(jobDTO.getType());
                    newJob.setSalary(Integer.parseInt(jobDTO.getSalary()));
                    newJob.setListedDate(new Date());
                    newJob.setLocation(jobDTO.getLocation());
                    newJob.setDepartment(dept);

                    jobRepo.save(newJob);
            return true;
        }
        catch(Exception e){
            throw new Exception("FAILED TO CREATE JOB", e);
        }
    }

    public boolean editJob(JobDTO jobDTO)throws Exception{
        try{
            Jobs jobs = jobRepo.findById(jobDTO.getJobId()).orElse(null);
            jobs.setSalary(Integer.parseInt(jobDTO.getSalary()));
            jobs.setName(jobDTO.getName());
            jobs.setRequirements(jobDTO.getRequirements());
            jobs.setDescription(jobDTO.getDescription());
            jobs.setType(jobDTO.getType());
            jobs.setLocation(jobDTO.getLocation());
            jobs.setListedDate(new Date());

            jobRepo.save(jobs);

            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO EDIT JOB", e);
        }
    }

    public boolean deleteJob(String id)throws Exception{
        try{
            Jobs j = jobRepo.findById(id).orElse(null);
            for(Client c : j.getApplied()){
                c.getMyJobs().remove(j);
                j.getApplied().remove(c);
            }
//            jobRepo.save(j);
            jobRepo.delete(j);
            return true;
        }catch(Exception e){
            throw new Exception("FAIELD TO DELETE", e);
        }
    }

    public List<JobDTO> search(String searchkeyword, String location)throws Exception {
        try{
            List<Jobs> jobs = new ArrayList<>();
            if(searchkeyword==null && location!=null){
                jobs = jobRepo.findAllByLocationContaining(location);
            }else if(searchkeyword!=null && location==null){
                jobs = jobRepo.findAllByNameContaining(searchkeyword);
            }else if(searchkeyword!=null && location!=null){
                jobs = jobRepo.findAllByNameContainingAndLocationContaining(searchkeyword,location);
            }

            List<JobDTO> dto = new ArrayList<>();
            jobs.forEach(j -> {
                JobDTO job = new JobDTO();
                job.setJobId(j.getJobId());
                job.setName(j.getName());
                job.setDescription(j.getDescription());
                job.setRequirements(j.getRequirements());
                job.setType(j.getType());
                job.setSalary(String.valueOf(j.getSalary()));
                job.setLocation(j.getLocation());
                job.setListedDate(j.getListedDate());
                job.setCompany(j.getDepartment().getCompany().getCompanyName());

                dto.add(job);
            });

            return dto;

        }catch (Exception e){
            throw new Exception("FAILED THE SEARCH", e);
        }
    }

    public boolean applyForJob(JobApplication jobApplication) throws Exception{
        try{
            Client client = clientRepo.findByEmail(jobApplication.getEmail());
            Jobs job = jobRepo.findById(jobApplication.getJobId()).orElse(null);

            Set<Client> sc = job.getApplied();
            Set<Jobs> sj = client.getMyJobs();

            sc.add(client);
            sj.add(job);

            client.setMyJobs(sj);
            job.setApplied(sc);

            clientRepo.save(client);
            jobRepo.save(job);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO PROCESS APPLICATION", e);
        }
    }

    public List<Applicants> getAllApplicants(String email)throws Exception{
        try {
            Company company = companyRepo.findByEmail(email);
            List<Applicants> applicants = new ArrayList<>();

            company.getDepartmentList().forEach(department -> {
                department.getJobs().forEach(jobs -> {

                    List<ClientProfileDTO> clientList= new ArrayList<>();
                    jobs.getApplied().forEach(client -> {
                        clientList.add(
                            new ClientProfileDTO(
                                    client.getFirstName(),
                                    client.getLastName(),
                                    client.getEmail()
                            )
                        );
                    });

                    applicants.add(
                        new Applicants(
                                jobs.getJobId(),
                                jobs.getName(),
                                jobs.getDepartment().getDeptName(),
                                clientList
                        )
                    );

                });
            });

            return applicants;
        }
        catch (Exception e){
            throw new Exception("ERROR RETRIEVING APPLICANT DATA", e);
        }

    }

    public JobDTO getJob(String id) throws  Exception{
        try{
            Jobs j =jobRepo.findById(id).orElse(null);
            JobDTO job = new JobDTO();
            job.setJobId(j.getJobId());
            job.setName(j.getName());
            job.setDescription(j.getDescription());
            job.setRequirements(j.getRequirements());
            job.setType(j.getType());
            job.setSalary(String.valueOf(j.getSalary()));
            job.setLocation(j.getLocation());
            job.setListedDate(j.getListedDate());
            job.setDeptId(j.getDepartment().getDeptName());
            job.setCompany(j.getDepartment().getCompany().getCompanyName());

            return job;
        }
        catch (Exception e){
            throw new Exception("FAILEDTO GET JOB", e);
        }
    }

    public List<JobDTO> getAllJobs() throws Exception{
        try{

            List<Jobs> jobs = (List<Jobs>) jobRepo.findAllByOrderByJobIdDesc();
            List<JobDTO> dto = new ArrayList<>();
            jobs.forEach(j -> {
                JobDTO job = new JobDTO();
                job.setJobId(j.getJobId());
                job.setName(j.getName());
                job.setDescription(j.getDescription());
                job.setRequirements(j.getRequirements());
                job.setType(j.getType());
                job.setSalary(String.valueOf(j.getSalary()));
                job.setLocation(j.getLocation());
                job.setListedDate(j.getListedDate());
                job.setCompany(j.getDepartment().getCompany().getCompanyName());

                dto.add(job);
            });

            return dto;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET ALL JOBS", e);
        }
    }
}
