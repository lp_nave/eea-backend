package lk.apiit.eea.assignment.workforce.repos;

import lk.apiit.eea.assignment.workforce.model.Authentication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthenticationRepo extends CrudRepository<Authentication,String> {
    Authentication findByEmail(String email);
}

