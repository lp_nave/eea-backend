package lk.apiit.eea.assignment.workforce.repos;

import lk.apiit.eea.assignment.workforce.model.Ratings;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface RatingRepo extends CrudRepository<Ratings,Integer> {
    @Query(value = "SELECT  MAX(number_of_people)", nativeQuery = true)
    int findMaxPeople();

    List<Ratings> findByOrderByPeopleDesc();
//    List<Integer> findByPeopleOrderByPeopleDesc();

    @Query(value = "Select max(number_of_people) from eea.ratings", nativeQuery = true)
    int findMax();

}
