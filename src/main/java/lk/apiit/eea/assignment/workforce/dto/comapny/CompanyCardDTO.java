package lk.apiit.eea.assignment.workforce.dto.comapny;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CompanyCardDTO {
    private int companyId;
    private String companyName;
    private String location;
    private int jobs;
    private String description;
    private String image;
    private String imagePath;
    private double rating;
}
