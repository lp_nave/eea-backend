package lk.apiit.eea.assignment.workforce.services;

import lk.apiit.eea.assignment.workforce.config.JwtTokenUtil;
import lk.apiit.eea.assignment.workforce.dto.AuthenticateRequest;
import lk.apiit.eea.assignment.workforce.dto.JwtToken;
import lk.apiit.eea.assignment.workforce.dto.client.RegisterClientDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.RegisterCompanyDTO;
import lk.apiit.eea.assignment.workforce.model.Authentication;
import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Ratings;
import lk.apiit.eea.assignment.workforce.repos.AuthenticationRepo;
import lk.apiit.eea.assignment.workforce.repos.ClientRepo;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.repos.RatingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Service
@Transactional
public class AuthenticationService implements UserDetailsService {
    @Autowired
    private AuthenticationRepo authenticationRepo;

    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RatingRepo ratingRepo;

    @Autowired
    private ClientRepo clientRepo;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Authentication auth = authenticationRepo.findByEmail(email);
        if(auth== null){
            throw new UsernameNotFoundException("User not found with the email " + email);
        }
        return new org.springframework.security.core.userdetails.User( auth.getEmail(),auth.getPassword(),
                new ArrayList<>());
    }


    public ResponseEntity<?> authenticateUser(AuthenticateRequest auth) throws Exception {
        authenticate(auth.getEmail(),auth.getPassword());
        Authentication authentication = authenticationRepo.findByEmail(auth.getEmail());
        final UserDetails userDetails = loadUserByUsername(auth.getEmail());
        final String token= jwtTokenUtil.generateToken(userDetails,authentication);
                return ResponseEntity.ok(new JwtToken(token));
    }


    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }


    public ResponseEntity<?> addNewClient(RegisterClientDTO client) throws Exception{
        try{
            if(authenticationRepo.findByEmail(client.getEmail())!=null){
                return ResponseEntity.ok().body("User already exists");
            }

            Authentication newAuth = new Authentication();
            newAuth.setEmail(client.getEmail());
            newAuth.setPassword(bcryptEncoder.encode(client.getPassword()));
            newAuth.setRole("client");

            Client newClient = new Client();
            newClient.setEmail(client.getEmail());
            newClient.setFirstName(client.getFirstName());
            newClient.setLastName(client.getLastName());
            newClient.setContactNumber(client.getContactNumber());
            newClient.setLocation(client.getLocation());

            newAuth.setClient(newClient);
            newClient.setAuthentication(newAuth);

            authenticationRepo.save(newAuth);
//            clientRepo.save(newClient);
            UserDetails details;
//            do{

//                authenticate(newAuth.getEmail(),newAuth.getPassword());
                final UserDetails userDetails = loadUserByUsername(newAuth.getEmail());
                details=userDetails;

//            }while (details==null);
            final String token =jwtTokenUtil.generateToken(details, newAuth);

            return ResponseEntity.ok(new JwtToken(token));

        }catch (Exception e){
            throw new Exception("Failed to create client", e);
        }
    }

    public ResponseEntity<?> addNewCompany(RegisterCompanyDTO company)throws Exception {
        try{
            if(authenticationRepo.findByEmail(company.getEmail())!=null){
                return ResponseEntity.ok().body("Company already exists");
            }
            Authentication newAuth = new Authentication();
            newAuth.setEmail(company.getEmail());
            newAuth.setPassword(bcryptEncoder.encode(company.getPassword()));
            newAuth.setRole("company");

            Company newCompany = new Company();
            newCompany.setCompanyName(company.getCompanyName());
            newCompany.setAddress(company.getAddress());
            newCompany.setNumber(company.getContactNumber());
            newCompany.setEmail(company.getEmail());
            newCompany.setNoOfEmployees(company.getNoOfEmployees());
            newCompany.setDescpription(company.getDescription());
            newCompany.setNoOfJobs(company.getNoOfJobs());
            newCompany.setLocation(company.getLocation());

            Ratings r = new Ratings();
            r.setPeople(0);
            r.setRating(0.0);
            r.setStarsReceived(0);
            r.setCompany(newCompany);

            newCompany.setRatings(r);
            newAuth.setCompany(newCompany);
            newCompany.setAuthentication(newAuth);


            authenticationRepo.save(newAuth);
//            ratingRepo.save(r);
//            companyRepo.save(newCompany);

//            authenticate(newAuth.getEmail(),newAuth.getPassword());
            final UserDetails userDetails = loadUserByUsername(newAuth.getEmail());

            final String token =jwtTokenUtil.generateToken(userDetails, newAuth);

            return ResponseEntity.ok(new JwtToken(token));

        }catch (Exception e){
            throw new Exception("Failed to create company", e);
        }
    }

    public void checkAdmin() {
       Authentication auth = authenticationRepo.findByEmail("admin");
       if(auth==null){
           Authentication adminAuth = new Authentication();
           adminAuth.setEmail("Admin");
           adminAuth.setPassword(bcryptEncoder.encode("admin"));
           adminAuth.setRole("admin");

           authenticationRepo.save(adminAuth);
       }
    }
}
