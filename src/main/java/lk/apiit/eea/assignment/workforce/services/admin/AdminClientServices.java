package lk.apiit.eea.assignment.workforce.services.admin;

import com.sun.deploy.util.SessionState;
import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileDTO;
import lk.apiit.eea.assignment.workforce.model.Authentication;
import lk.apiit.eea.assignment.workforce.model.Client;
import lk.apiit.eea.assignment.workforce.model.Jobs;
import lk.apiit.eea.assignment.workforce.repos.AuthenticationRepo;
import lk.apiit.eea.assignment.workforce.repos.ClientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AdminClientServices {

    @Autowired
    private ClientRepo clientRepo;

    @Autowired
    private AuthenticationRepo authenticationRepo;

    public List<ClientProfileDTO> getClients() {
        List<Client> clients = (List<Client>) clientRepo.findAll();

        List<ClientProfileDTO> list = new ArrayList<>();
        clients.forEach(client -> {

            ClientProfileDTO dto = new ClientProfileDTO();

            dto.setEmail(client.getEmail());
            dto.setFirstName(client.getFirstName());
            dto.setLastName(client.getLastName());
            dto.setLocation(client.getLocation());
            dto.setContactNumber(client.getContactNumber());
            dto.setSumJobs(client.getMyJobs().size());

            list.add(dto);
        });
           return list;
    }

    public List<Client> getCSVdata() {
        List<Client> rawData = (List<Client>) clientRepo.findAll();
        return rawData;

    }

    public boolean deleteClient(String email) throws Exception{

        try {
            Client c = clientRepo.findByEmail(email);
            for(Jobs j : c.getMyJobs()){
                j.getApplied().remove(c);
                c.getMyJobs().remove(j);
            }
            Authentication a = authenticationRepo.findByEmail(email);
            clientRepo.delete(c);
            authenticationRepo.delete(a);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO DELETE", e);
        }
    }
}
