package lk.apiit.eea.assignment.workforce.dto.comapny;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RatingDTO {
    private String companyID;
    private String noOfStars;
}
