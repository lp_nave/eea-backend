package lk.apiit.eea.assignment.workforce.services.company;

import lk.apiit.eea.assignment.workforce.dto.Job.JobDTO;
import lk.apiit.eea.assignment.workforce.dto.Job.ListingsDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.CompanyCardDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.RatingDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.RegisterCompanyDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.department.DepartmentDTO;
import lk.apiit.eea.assignment.workforce.dto.comapny.profile.CompanyProfileHeader;
import lk.apiit.eea.assignment.workforce.dto.comapny.profile.CompanySegment;
import lk.apiit.eea.assignment.workforce.dto.comapny.profile.ViewCompany;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Jobs;
import lk.apiit.eea.assignment.workforce.model.Ratings;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CompanyService {

    @Autowired
    private CompanyRepo companyRepo;
    @Autowired
    private PasswordEncoder bcryptEncoder;
    @Autowired
    private ImageService imageService;

    public List<CompanyCardDTO> getRandom() throws Exception{
        try{
            List<Company> dbList = companyRepo.getRandom();
            List<CompanyCardDTO> dtoList = new ArrayList<>();

            dbList.forEach(company -> {
                CompanyCardDTO card = new CompanyCardDTO();
                    if(company.getImage()!=null){
                        try {
                            String b64 = imageService.getB64(company.getImage(), "Profile");
                            card.setImage(b64);
                            card.setImagePath(company.getImage());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    card.setCompanyId(company.getId());
                    card.setCompanyName(company.getCompanyName());
                    card.setDescription(company.getDescpription());
                    card.setLocation(company.getLocation());
                    card.setJobs(company.getNoOfJobs());
                    card.setRating(company.getRatings().getRating());
                dtoList.add(card);
            });

            return dtoList;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET RANDOM COMPANIES", e);
        }
    }

    public CompanyProfileHeader getProfileHeader(String email)throws Exception{

        try{
            Company company = companyRepo.findByEmail(email);

            CompanyProfileHeader header = new CompanyProfileHeader();

            List<JobDTO> list = new ArrayList<>();

            if(company.getImage()!=null){
                try {
                    String b64 = imageService.getB64(company.getImage(), "Profile");
                    header.setImage(b64);
                    header.setImagePath(company.getImage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            header.setId(company.getId());
            header.setCompanyName(company.getCompanyName());
            header.setLocation(company.getLocation());
            header.setEmployee(company.getNoOfEmployees());
            header.setDepartments(company.getDepartmentList().size());
            header.setRating(company.getRatings().getRating());
            header.setJobs(company.getNoOfJobs());

            company.getDepartmentList().forEach(d->{
                d.getJobs().forEach(j->{
                    list.add(
                            new JobDTO(
                                    j.getJobId(),
                                    j.getName(),
                                    j.getType(),
                                    j.getListedDate()
                            )
                    );
                });
            });

            int sumJob=0;
            for(int i=0; i<company.getDepartmentList().size();i++){
//                sumJob = sumJob + company.getDepartmentList().get(i).getJobs().size();
                sumJob += company.getDepartmentList().get(i).getJobs().size();
            }
            header.setVacancies(sumJob);
            header.setJobList(list);

            return header;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET COMPANY HEADER", e);
        }

    }

    public List<DepartmentDTO> getDepartments(String email)throws Exception {
        try{
            Company c = companyRepo.findByEmail(email);
            List<DepartmentDTO> dto = new ArrayList<>();
            c.getDepartmentList().forEach(department -> {
                dto.add(new DepartmentDTO(
                        department.getId(),
                        department.getDeptName()
                ));
            });

            return dto;
        }
        catch (Exception e){
            throw new Exception("ERROR IN GETTING DEPARTMENTS", e);
        }
    }

    public List<JobDTO> getVacancies(String email)throws Exception {
        try{
            Company c = companyRepo.findByEmail(email);
            List<JobDTO> dto = new ArrayList<>();

            c.getDepartmentList().forEach(d -> {
                d.getJobs().forEach(j ->{
                    dto.add(
                            new JobDTO(
                                    j.getJobId(),
                                    j.getName(),
                                    j.getType(),
                                    j.getListedDate()
                            )
                    );
                } );
            });

            return dto;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET VACANCIES",e);
        }
    }

    public ViewCompany viewCompany(int id) throws Exception{
        try{
            Company c = companyRepo.findById(id).orElse(null);
            List<ListingsDTO> joblist = new ArrayList<>();
            c.getDepartmentList().forEach(department -> {
                department.getJobs().forEach(j -> {
                    joblist.add(
                            new ListingsDTO(
                                    j.getJobId(),
                                    j.getName(),
                                    j.getDescription(),
                                    j.getSalary(),
                                    j.getDepartment().getCompany().getCompanyName(),
                                    j.getLocation(),
                                    j.getListedDate()
                            )
                    );
                });
            });

            ViewCompany vc = new ViewCompany();

            if(c.getImage()!=null){
                try {
                    String b64 = imageService.getB64(c.getImage(), "Profile");
                    vc.setImage(b64);
                    vc.setImagePath(c.getImage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            vc.setId(c.getId());
            vc.setCompanyName(c.getCompanyName());
            vc.setEmail(c.getEmail());
            vc.setLocation(c.getLocation());
            vc.setContactNumber(c.getNumber());
            vc.setAddress(c.getAddress());
            vc.setJobs(c.getNoOfJobs());
            vc.setEmployee(c.getNoOfEmployees());
            vc.setDepartments(c.getDepartmentList().size());
            vc.setVacancies(joblist.size());
            vc.setJoblist(joblist);
            vc.setRating(c.getRatings().getRating());

            return vc;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET COMPANY DATA", e);
        }
    }

    public ViewCompany viewCompanyByEmail(String email) throws Exception{
        try{
            Company c = companyRepo.findByEmail(email);
            List<ListingsDTO> joblist = new ArrayList<>();
            c.getDepartmentList().forEach(department -> {
                department.getJobs().forEach(j -> {
                    joblist.add(
                            new ListingsDTO(
                                    j.getJobId(),
                                    j.getName(),
                                    j.getDescription(),
                                    j.getSalary(),
                                    j.getDepartment().getCompany().getCompanyName(),
                                    j.getLocation(),
                                    j.getListedDate()
                            )
                    );
                });
            });

            ViewCompany vc = new ViewCompany();

            if(c.getImage()!=null){
                try {
                    String b64 = imageService.getB64(c.getImage(), "Profile");
                    vc.setImage(b64);
                    vc.setImagePath(c.getImage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            vc.setId(c.getId());
            vc.setCompanyName(c.getCompanyName());
            vc.setEmail(c.getEmail());
            vc.setLocation(c.getLocation());
            vc.setContactNumber(c.getNumber());
            vc.setAddress(c.getAddress());
            vc.setJobs(c.getNoOfJobs());
            vc.setEmployee(c.getNoOfEmployees());
            vc.setDepartments(c.getDepartmentList().size());
            vc.setVacancies(joblist.size());
            vc.setJoblist(joblist);
            vc.setRating(c.getRatings().getRating());

            return vc;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET COMPANY DATA", e);
        }
    }

    public boolean editCompany(RegisterCompanyDTO dto) throws Exception{
        try{
            Company c = companyRepo.findByEmail(dto.getEmail());
            c.setCompanyName(dto.getCompanyName());
            c.setAddress(dto.getAddress());
            c.setDescpription(dto.getDescription());
            c.setLocation(dto.getLocation());
            c.setNoOfJobs(dto.getNoOfJobs());
            c.setNoOfEmployees(dto.getNoOfEmployees());
            c.setNumber(dto.getContactNumber());

            if(dto.getPassword()!=null){
                c.getAuthentication().setPassword(bcryptEncoder.encode(dto.getPassword()));
            }

            companyRepo.save(c);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO EDIT COMPANY", e);
        }
    }

    public CompanySegment loadCompanyProfileSegment(String email)throws Exception {
        try{
            Company c = companyRepo.findByEmail(email);

            List<DepartmentDTO> departments = new ArrayList<>();
            List<JobDTO> jobs = new ArrayList<>();
            CompanySegment cs = new CompanySegment();

            if(c.getImage()!=null){
                String b64 = imageService.getB64(c.getImage(), "Profile");
                cs.setImage(b64);
                cs.setImagePath(c.getImage());
            }

            c.getDepartmentList().forEach(d->{
                departments.add(
                        new DepartmentDTO(
                                d.getId(),
                                d.getDeptName()
                        )
                );
                d.getJobs().forEach(j->{
                    jobs.add(
                            new JobDTO(
                                    j.getJobId(),
                                    j.getName(),
                                    j.getType(),
                                    j.getListedDate()
                            )
                    );
                });
            });

            Collections.sort(jobs);

//            Arrays.sort(jobs, new Comparator<JobDTO>(){
//                public int compare(JobDTO j1 , JobDTO j2 ){
//                    return Long.valueOf(j1.getListedDate().toString()).compareTo(j2.getListedDate());
//                }
//            });

            List<JobDTO> latest = new ArrayList<>();
            if(jobs.size()>0){
                for(int i=1;i<=3;i++){
                    if(i<=jobs.size()){
                        latest.add(jobs.get(jobs.size()-i));
                    }
                }
//
            }
//            latest.add(jobs.get(jobs.size()-1));
//                latest.add(jobs.get(jobs.size()-2));
//                latest.add(jobs.get(jobs.size()-3));

            cs.setCompanyName(c.getCompanyName());
            cs.setLocation(c.getLocation());
            cs.setDeparments(departments);
            cs.setJobs(latest);

            return cs;
        }
        catch ( Exception e){
            throw new Exception("ERROR IN LOADING COMPANY DATA FOR SEGMENT", e);
        }
    }

    public RegisterCompanyDTO getCompany(String email)throws  Exception{
        try{
            Company c =  companyRepo.findByEmail(email);
            RegisterCompanyDTO dto = new RegisterCompanyDTO();
            dto.setEmail(c.getEmail());
            dto.setCompanyName(c.getCompanyName());
            dto.setAddress(c.getAddress());
            dto.setContactNumber(c.getNumber());
            dto.setNoOfEmployees(c.getNoOfEmployees());
            dto.setNoOfJobs(c.getNoOfJobs());
            dto.setDescription(c.getDescpription());
            dto.setLocation(c.getLocation());

            return dto;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET COMPANY DATA", e);
        }
    }

    public List<CompanyCardDTO> getAll()throws Exception {
        try{
            List<Company> dbList = (List<Company>) companyRepo.findAll();
            List<CompanyCardDTO> dtoList = new ArrayList<>();

            dbList.forEach(company -> {

                CompanyCardDTO card = new CompanyCardDTO();

                if(company.getImage()!=null){
                    try {
                        String b64 = imageService.getB64(company.getImage(), "Profile");
                        card.setImage(b64);
                        card.setImagePath(company.getImage());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                card.setCompanyId(company.getId());
                card.setCompanyName(company.getCompanyName());
                card.setLocation(company.getLocation());
                card.setJobs(company.getNoOfJobs());
                card.setDescription( company.getDescpription());
                card.setRating(company.getRatings().getRating());

                dtoList.add(card);
            });

            return dtoList;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET RANDOM COMPANIES", e);
        }
    }

    public List<CompanyCardDTO> search(String companyName, String location) throws Exception{
        try{
            List<Company> searchList = new ArrayList<>();
            if(companyName==null && location!=null){
                searchList = companyRepo.findAllByLocationContaining(location);
            }else if(companyName!=null && location==null){
                searchList = companyRepo.findAllByCompanyNameContaining(companyName);
            }else if(companyName!=null && location!=null){
                searchList = companyRepo.findAllByCompanyNameContainingAndLocationContaining(companyName,location);
            }

            List<CompanyCardDTO> respondingList= new ArrayList<>();
            searchList.forEach(company-> {

                        CompanyCardDTO card = new CompanyCardDTO();

                        if(company.getImage()!=null){
                            try {
                                String b64 = imageService.getB64(company.getImage(), "Profile");
                                card.setImage(b64);
                                card.setImagePath(company.getImage());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        card.setCompanyId(company.getId());
                        card.setCompanyName(company.getCompanyName());
                        card.setLocation(company.getLocation());
                        card.setJobs(company.getNoOfJobs());
                        card.setDescription( company.getDescpription());
                        card.setRating(company.getRatings().getRating());


                respondingList.add(card);

            });

            return respondingList;

        }catch (Exception e){
            throw new Exception("FAILED THE SEARCH", e);
        }

    }

    public List<JobDTO> getMobileCompanyVacancie(int id) throws Exception {
        try{
            Company c = companyRepo.findById(id).orElse(null);
            List<JobDTO> dto = new ArrayList<>();

            c.getDepartmentList().forEach(d -> {
                d.getJobs().forEach(j ->{
                    JobDTO job = new JobDTO();
                    job.setJobId(j.getJobId());
                    job.setName(j.getName());
                    job.setDescription(j.getDescription());
                    job.setRequirements(j.getRequirements());
                    job.setType(j.getType());
                    job.setSalary(String.valueOf(j.getSalary()));
                    job.setLocation(j.getLocation());
                    job.setListedDate(j.getListedDate());
                    job.setCompany(j.getDepartment().getCompany().getCompanyName());

                    dto.add(job);
                } );
            });

            return dto;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET VACANCIES",e);
        }
    }

    public List<JobDTO> getMobileCompanyVacanciebyEmail(String email) throws Exception {
        try{
            Company c = companyRepo.findByEmail(email);
            List<JobDTO> dto = new ArrayList<>();

            c.getDepartmentList().forEach(d -> {
                d.getJobs().forEach(j ->{
                    JobDTO job = new JobDTO();
                    job.setJobId(j.getJobId());
                    job.setName(j.getName());
                    job.setDescription(j.getDescription());
                    job.setRequirements(j.getRequirements());
                    job.setType(j.getType());
                    job.setSalary(String.valueOf(j.getSalary()));
                    job.setLocation(j.getLocation());
                    job.setListedDate(j.getListedDate());
                    job.setCompany(j.getDepartment().getCompany().getCompanyName());

                    dto.add(job);
                } );
            });

            return dto;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET VACANCIES",e);
        }
    }
}
