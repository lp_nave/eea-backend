package lk.apiit.eea.assignment.workforce.dto.comapny.department;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentDTO {
    private String id;
    private String departmentName;
    private int companyId;
    private String email;



    public DepartmentDTO(String id, String departmentName, int companyId) {
        this.id = id;
        this.departmentName = departmentName;
        this.companyId = companyId;
    }

    public DepartmentDTO(String id, String departmentName) {
        this.id = id;
        this.departmentName = departmentName;
    }
}
