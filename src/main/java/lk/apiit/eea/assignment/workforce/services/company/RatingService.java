package lk.apiit.eea.assignment.workforce.services.company;

import lk.apiit.eea.assignment.workforce.dto.comapny.RatingDTO;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Ratings;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.repos.RatingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RatingService {

    @Autowired
    private RatingRepo ratingRepo;

    @Autowired
    private CompanyRepo companyRepo;


//    public boolean setRatings(RatingDTO dto)throws Exception {
//        try {
//            int maxPeople = ratingRepo.findMaxPeople();
//            if(maxPeople==0){
//                maxPeople=1;
//            }
//            Company c = companyRepo.findById(Integer.parseInt(dto.getCompanyID())).orElse(null);
//            Ratings r = c.getRatings();
//            int stars= r.getStarsReceived()+ Integer.parseInt(dto.getNoOfStars());
//            int people = r.getPeople() + 1;
//
//            int percentage = ((stars/(people*5))*(people/maxPeople))*100;
//
//            double newRating = percentage/20;
//
//            r.setPeople(people);
//            r.setRating(newRating);
//            r.setPeople(people);
//            ratingRepo.save(r);
//
//            return true;
//        }
//        catch (Exception e){
//            throw new Exception("ERROR CALCULATING RATING");
//        }
//    }
    public boolean setRatings(String id, String rating)throws Exception {
        try {
            List<Ratings> list = ratingRepo.findByOrderByPeopleDesc();
//            List<Integer> max = ratingRepo.findByPeopleOrderByPeopleDesc();
            int max = ratingRepo.findMax();

//            int maxPeople = list.get(0).getPeople();
            int maxPeople = max;
            if(maxPeople==0){
                maxPeople=1;
            }
            Company c = companyRepo.findById(Integer.parseInt(id)).orElse(null);
            Ratings r = c.getRatings();
            int stars= r.getStarsReceived()+ Integer.parseInt(rating);
            if(stars==0){
                stars=1;
            }
            int people = r.getPeople() + 1;

//            int percentage = (((stars/(people*5))*(people/maxPeople))*100);

//            double newRating = percentage/20;
            double newRating = stars/maxPeople;

            r.setPeople(people);
            r.setRating(newRating);
            r.setPeople(people);
            r.setStarsReceived(stars);
            ratingRepo.save(r);

            return true;
        }
        catch (Exception e){
            throw new Exception("ERROR CALCULATING RATING");
        }
    }
}
