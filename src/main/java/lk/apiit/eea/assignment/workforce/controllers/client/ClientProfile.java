package lk.apiit.eea.assignment.workforce.controllers.client;

import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileDTO;
import lk.apiit.eea.assignment.workforce.dto.client.ClientProfileSegmentDTO;
import lk.apiit.eea.assignment.workforce.dto.client.RegisterClientDTO;
import lk.apiit.eea.assignment.workforce.services.client.ClientProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientProfile {
    @Autowired
    private ClientProfileService clientProfileService;

    @GetMapping("/clientProfileSegment/{email}")
    public ClientProfileSegmentDTO getClientHomepageData(@PathVariable String email) throws  Exception {
        return clientProfileService.getSegment(email);
    }

    @GetMapping("/clientProfile/{email}")
    public ClientProfileDTO getClientProfile(@PathVariable String email)throws Exception{
        return clientProfileService.getProfile(email);
    }

    @GetMapping("/getClient/{email}")
    public RegisterClientDTO getClient(@PathVariable String email)throws Exception{
        return clientProfileService.getClient(email);
    }

    @PutMapping("/editClientProfile")
    public boolean editClientProfile(@RequestBody RegisterClientDTO dto)throws Exception{
        return clientProfileService.editProfile(dto);
    }

}
