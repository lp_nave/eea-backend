package lk.apiit.eea.assignment.workforce.controllers.admin;

import lk.apiit.eea.assignment.workforce.csv.WriteToCSV;
import lk.apiit.eea.assignment.workforce.dto.comapny.CompanyCardDTO;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.services.admin.AdminCompanyServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
public class AdminCompanyController {

    @Autowired
    private AdminCompanyServices services;

    @GetMapping("/adminCompanies")
    public List<CompanyCardDTO> adminCompanies(){
        return services.adminCompanies();
    }


    @GetMapping("/CompanyCSV")
    public void downloadCompanies(HttpServletResponse response) throws IOException {
        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; file=customers.csv");

        List<Company> companies = services.getCSVdata();
        WriteToCSV.writeCompanyDataToCsv(response.getWriter(), companies);
    }

    @DeleteMapping("/DeleteCompany/{id}")
    public boolean deleteCompany(@PathVariable int id)throws Exception{
        return services.deleteCompany(id);
    }

}
