package lk.apiit.eea.assignment.workforce.dto.Job;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class JobApplication {
    private String jobId;
    private String email;
}
