package lk.apiit.eea.assignment.workforce.services.company;

import lk.apiit.eea.assignment.workforce.dto.comapny.department.DepartmentDTO;
import lk.apiit.eea.assignment.workforce.model.Company;
import lk.apiit.eea.assignment.workforce.model.Department;
import lk.apiit.eea.assignment.workforce.repos.CompanyRepo;
import lk.apiit.eea.assignment.workforce.repos.DepartmentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DepartmentService {
    @Autowired
    private DepartmentRepo departmentRepo;

    @Autowired
    private CompanyRepo companyRepo;

    public boolean addDept(DepartmentDTO dept) throws Exception{
        try{
            Company company = companyRepo.findByEmail(dept.getEmail());
            Department department = new Department();
            department.setCompany(company);
            department.setDeptName(dept.getDepartmentName());

            departmentRepo.save(department);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO SAVE DEPARTMENT", e);
        }
    }

    public boolean deleteDept(String id)throws Exception{
        try {
            departmentRepo.deleteById(id);
            return true;
        }
        catch (Exception e){
            throw new Exception("FAILED TO DELETE DEPARTMENT",e );
        }
    }

    public List<DepartmentDTO> getAll(String email)throws Exception{
        try{
            Company company = companyRepo.findByEmail(email);
            List<DepartmentDTO> list = new ArrayList<>();
            company.getDepartmentList().forEach(department -> {
                list.add(
                        new DepartmentDTO(
                                department.getId(),
                                department.getDeptName()
                        )
                );
            });
            return list;
        }
        catch (Exception e){
            throw new Exception("FAILED TO GET ALL DEPARTMENTS", e)
;        }
    }

    public List<DepartmentDTO> listAll (){
        List<DepartmentDTO> dto = new ArrayList<>();
        List<Department> d = (List<Department>) departmentRepo.findAll();
        d.forEach(department -> {
            DepartmentDTO dt = new DepartmentDTO();
            dt.setDepartmentName(department.getDeptName());
            dt.setId(department.getId());
            dt.setCompanyId(department.getCompany().getId());
            dt.setEmail(department.getCompany().getEmail());
            dto.add(dt);
        });
        return dto;
    }
}
